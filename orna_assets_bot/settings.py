import logging
import os
import re
from logging import config
try:
    import settings_local
except ImportError as exc:
    import traceback
    traceback.print_exc()
    settings_local = object()


TOKEN = getattr(settings_local, 'TOKEN', '')
OCR_SPACE_API_KEY = getattr(settings_local, 'OCR_SPACE_API_KEY', '')
OCR_SPACE_API_URL = "https://api.ocr.space/parse/image"
LOG_LEVEL = getattr(settings_local, 'LOG_LEVEL', 'DEBUG')
LOG_FILENAME = getattr(settings_local, 'LOG_FILENAME', 'orna.log')
DEVELOPER_CHAT_ID = getattr(settings_local, 'DEVELOPER_CHAT_ID', '')

GREETING_TEXT = 'WELCOME new user'
USERS_PER_MESSAGE = 4

groups_file = 'groups.json'
group_welcomes_file = 'group_welcomes.json'
user_nicks_file = 'user_nicks.json'

media_dir = '/tmp/orna_media'


def get_media_path(media_group_id):
    path = os.path.join(media_dir, media_group_id)
    os.makedirs(path, exist_ok=True)
    return path


logFormatter = "%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s] " \
               "%(message)s"


logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,  # this fixes the problem
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'default': {
            'class': 'logging.StreamHandler',
            'level': LOG_LEVEL,
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': LOG_LEVEL,
            'filename': LOG_FILENAME,
            'maxBytes': 2000,
            'backupCount': 5,
        }
    },
    'loggers': {
        '': {
            'handlers': ['default', 'file'],
            'level': LOG_LEVEL,
            'propagate': True
        }
    }
})


def save_groups(groups):
    import json
    with open(groups_file, 'w') as fh:
        json.dump(groups, fh)


def load_json(file_path):
    import os
    import json
    groups = {}
    if os.path.exists(file_path):
        with open(file_path) as fh:
            groups = json.load(fh)
    return groups


def extend_groups(groups: dict, text: str,
                  subgroup_len: int = USERS_PER_MESSAGE):
    if '@' not in text:
        return []

    user_list = set()

    for group_name in re.findall(r'(@\w+)', text):
        user_list.update(groups.get(group_name[1:].lower(), []))

    if not user_list:
        return []

    users_by_subgroups = []
    user_list = list(user_list)
    user_list.sort()
    users_count = len(user_list)
    if users_count <= subgroup_len:
        users_by_subgroups.append(user_list)
    else:
        for i in range(users_count // subgroup_len):
            users_by_subgroups.append(
                user_list[subgroup_len * i: subgroup_len * i + subgroup_len]
            )
        if users_count % subgroup_len != 0:
            users_by_subgroups.append(
                user_list[subgroup_len * (users_count // subgroup_len):]
            )

    plain_results = []
    for users in users_by_subgroups:
        plain_results.append(text + ' ' + ' '.join([u for u in users]))
    return plain_results

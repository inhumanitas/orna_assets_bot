import logging
import html
import json
import os
import traceback

from telegram import Chat
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram.parsemode import ParseMode

import settings
import image_generator
from orna_assets import get_data_by_image

logger = logging.getLogger(__name__)

GROUP_TYPES = (Chat.GROUP, Chat.SUPERGROUP)

GROUPS = {}

GROUP_WELCOME_MESSAGES = {}

USER_NICKS = {}  # [user tg nick]: [user orna nick]


def start(update, context):
    """Первая команда"""
    logger.info('New user %s',  update.message.from_user)
    update.message.reply_text(
        'Привет! Пришли мне скрин шмотки в виде файла (несжатое), '
        'и я попробую его распознать.')


def group_add(update, context):
    """Добавить подгруппу пользователей"""
    text = update.message.text
    group_id = str(update.message.chat.id)
    logger.info('Entering group_add')
    if update.message.chat.type not in GROUP_TYPES or group_id not in GROUPS:
        logger.info(f'Unauthorized group add command by {group_id}')
        return

    words = ' '.join(filter(bool, text.split(' '))).split(' ')  # remove multiple spaces
    if len(words) != 2:
        update.message.reply_text(
            f'Добавить группу:\n/group_add <название группы>')
        return

    _cmd, new_group = words

    if new_group in GROUPS[group_id]:
        update.message.reply_text(
            'Группа существует. Посмотреть список /group_list')
    elif new_group:
        GROUPS[group_id][new_group] = []
        settings.save_groups(GROUPS)
        update.message.reply_text(
            f'Добавил: {new_group}. Посмотреть список /group_list')
    else:
        update.message.reply_text(
            f'Добавить группу:\n/group_add <название группы>')


def group_list(update, context):
    """Распечатать подгруппы"""
    group_id = str(update.message.chat.id)
    logger.info('Entering group_list')
    if update.message.chat.type not in GROUP_TYPES or group_id not in GROUPS:
        logger.info(f'Unauthorized group list command by {group_id}')
        return

    text = ''
    ident = ' ' * 4

    for group in sorted(GROUPS[group_id]):
        users = sorted(GROUPS[group_id][group])
        text += '<b>' + group.capitalize() + ' (' + str(len(users)) + '):</b>\n'
        text += '\n'.join([ident+u.replace('@', '') for u in users]) + '\n\n'
    if text:
        update.message.reply_html(text)


def group_user_add(update, context):
    """Добавить пользователей в подгруппу"""
    text = update.message.text
    group_id = str(update.message.chat.id)
    logger.info('Entering group_user_add')
    if update.message.chat.type not in GROUP_TYPES or group_id not in GROUPS:
        logger.info(f'Unauthorized group user add command by {group_id}')
        return

    words = ' '.join(filter(bool, text.split(' '))).split(' ')  # remove multiple spaces
    if len(words) >= 3:
        _cmd, group_name, *user_names = words
        if group_name.startswith('@'):
            group_name = group_name[1:]
        if group_name not in GROUPS[group_id]:
            update.message.reply_text('Требуется создать группу')
            return

        results = []
        for user_name in user_names:
            if not user_name.startswith('@'):
                user_name = '@' + user_name

            if user_name in GROUPS[group_id][group_name]:
                results.append(f'{user_name} - уже в группе')
                continue

            GROUPS[group_id][group_name].append(user_name)
            results.append(f'Добавил {user_name} в группу')

        update.message.reply_text('\n'.join(results))
        settings.save_groups(GROUPS)
        return

    update.message.reply_text(
        'Добавить пользователя в групппу: <имя группы> <имя пользователя>\n'
        'Пример:\n/group_user_add group1 @user2'
    )


def group_user_del(update, context):
    """Удалить пользователя из подгруппы"""
    text = update.message.text
    group_id = str(update.message.chat.id)
    logger.info('Entering group_user_del')
    if update.message.chat.type not in GROUP_TYPES or group_id not in GROUPS:
        logger.info(f'Unauthorized group user del command by {group_id}')
        return

    words = ' '.join(filter(bool, text.split(' '))).split(' ')  # remove multiple spaces
    if len(words) >= 3:
        _cmd, group_name, *user_names = words
        if group_name.startswith('@'):
            group_name = group_name[1:]
        if group_name in GROUPS[group_id]:
            results = []
            for user_name in user_names:
                if not user_name.startswith('@'):
                    user_name = '@' + user_name

                if user_name in GROUPS[group_id][group_name]:
                    GROUPS[group_id][group_name].remove(user_name)
                    results.append(f'Уволил {user_name} из группы')
                else:
                    results.append(f'Не нашел {user_name} в группе.')
            settings.save_groups(GROUPS)
            update.message.reply_text('\n'.join(results))
            return

    update.message.reply_text(
        'Удалить пользователя из групппы: <имя группы> <имя пользователя>\n'
        'Пример:\n/group_user_del group1 @user2'
    )


def error_handler(update, context):
    """Log the error and send a telegram message to notify the developer."""
    # Log the error before we do anything else, so we can see it even if something breaks.
    logger.error(msg="Exception while handling an update:", exc_info=context.error)

    # traceback.format_exception returns the usual python message about an exception, but as a
    # list of strings rather than a single string, so we have to join them together.
    tb_list = traceback.format_exception(None, context.error, context.error.__traceback__)
    tb = ''.join(tb_list)

    # Build the message with some markup and additional information about what happened.
    # You might need to add some logic to deal with messages longer than the 4096 character limit.
    message = (
        'An exception was raised while handling an update\n'
        '<pre>update = {}</pre>\n\n'
        '<pre>context.chat_data = {}</pre>\n\n'
        '<pre>context.user_data = {}</pre>\n\n'
        '<pre>{}</pre>'
    ).format(
        html.escape(json.dumps(update.to_dict(), indent=2, ensure_ascii=False)) if update else 'UNEXPECTED',
        html.escape(str(context.chat_data)),
        html.escape(str(context.user_data)),
        html.escape(tb)
    )

    # Finally, send the message
    context.bot.send_message(
        chat_id=settings.DEVELOPER_CHAT_ID,
        text=message,
        parse_mode=ParseMode.HTML
    )


def any_text(update, context):
    if update.edited_message:
        message = update.edited_message
    else:
        message = update.message

    group_id = str(message.chat.id)

    if message.chat.type in GROUP_TYPES:
        if group_id in GROUPS:
            logger.info(
                f'Got text from by {message.from_user}\n{message.text}')
            text = message.text

            for prepared_text in settings.extend_groups(GROUPS[group_id], text):
                message.reply_text(prepared_text)
            if group_id in USER_NICKS:
                for prepared_text in settings.extend_groups(USER_NICKS[group_id], text):
                    message.reply_text(prepared_text)
        else:
            GROUPS[group_id] = {}
            settings.save_groups(GROUPS)


def sent_photo(update, context):
    if not update.message or update.message.chat.type in GROUP_TYPES:
        return
    logger.info('User sent photo %s', update.message.from_user)
    update.message.reply_text(
        'Фото нужно в виде файла. Шакалы не дремлют.')


def file_photo(update, context):

    if not update.message:
        return

    logger.info('User uploaded photo %s',  update.message.from_user)
    media_group_id = update.message.media_group_id
    if (media_group_id and update.message.document and
            update.message.document.mime_type == 'image/jpeg'):

        path = settings.get_media_path(media_group_id)
        file_path = os.path.join(path, update.message.document.file_name)

        uploaded_path = update.message.document.get_file().download(file_path)
        logger.info('User uploaded photo %s - %s',
                    update.message.from_user, uploaded_path)

    else:
        if update.message.chat.type in GROUP_TYPES:
            return

        username = update.message.from_user.username
        update.message.reply_text('Минутку...', parse_mode=ParseMode.MARKDOWN)
        media_path = settings.get_media_path(username)
        file_path = os.path.join(media_path, update.message.document.file_name)
        try:
            update.message.document.get_file().download(file_path)
        except Exception as e:
            logger.error(e)
            context.bot.send_message(
                chat_id=settings.DEVELOPER_CHAT_ID,
                text=f'Не смог сохранить файл, от {username}'
                     f'\n' + str(e),
            )
            update.message.reply_text('Ой, не сегодня',
                                      parse_mode=ParseMode.MARKDOWN)
            return

        error = reply = None
        logger.info(f'Parsing {file_path}')
        try:
            reply = get_data_by_image(file_path)
        except ValueError as e:
            logger.error(e)
            error = str(e)
        except Exception as e:
            logger.error(e)
            error = str(e)

        logger.info(f'Parsed {file_path} as\n{reply}')
        if error:
            context.bot.send_message(
                chat_id=settings.DEVELOPER_CHAT_ID,
                text=f'Не смог распарсить файл, от {username}\n'
                     f'Путь до файла {file_path}\n' + str(error),
            )
            reply = 'Не шмогла.. 😓'
        update.message.reply_text(reply, parse_mode=ParseMode.MARKDOWN)


def welcome(update, context):
    new_chat_members = update.message.new_chat_members
    logger.info(f'New users: {new_chat_members}')
    group_id = str(update.message.chat.id)
    master_group_id, group_welcome_message = GROUP_WELCOME_MESSAGES.get(group_id)
    if group_welcome_message:
        user_names = ''
        post_text = ''

        for user in new_chat_members:
            user_names += user.first_name + '! '
            if not user.username:
                post_text += user.first_name + ' пропиши `username` в telegram! '

        group_welcome_message = ' '.join(
            [user_names, group_welcome_message, post_text])

        context.bot.send_message(
            chat_id=settings.DEVELOPER_CHAT_ID,
            text=group_welcome_message,
            # parse_mode=ParseMode.HTML
        )
        context.bot.send_message(
            chat_id=group_id,
            text=group_welcome_message,
            # parse_mode=ParseMode.HTML
        )
        if master_group_id:
            context.bot.send_message(
                chat_id=master_group_id,
                text=(
                    'Тук-тук, ' + (
                        'новый игрок: '
                        if len(new_chat_members) == 1 else 'новые игроки: '
                    ) + user_names
                ),
                # parse_mode=ParseMode.HTML
            )


def help_cmd(update, context):
    """Показать список комманд"""
    reply = 'Список доступных комманд:\n'
    for cmd_name, cmd_fn in commands:
        cmd_help = '  /{cmd_name}{doc}\n'
        # FIXME use special function attr for docs
        doc = (" - " + cmd_fn.__doc__) if cmd_fn.__doc__ else ''
        reply += cmd_help.format(cmd_name=cmd_name, doc=doc)

    update.message.reply_text(reply)


def make_photo(update, context):

    if not update.message.reply_to_message:
        return

    try:
        requested_doc_type = update.message.reply_to_message.document.mime_type
        media_group_id = update.message.reply_to_message.media_group_id
    except AttributeError:
        return

    if requested_doc_type == 'image/jpeg':
        update.message.reply_text('Минутку...', parse_mode=ParseMode.MARKDOWN)

        caption = update.message.reply_to_message.caption
        out_image = image_generator.create_image(media_group_id)
        if out_image:
            update.message.reply_document(
                document=open(out_image, 'rb'), caption=caption)
        else:
            update.message.reply_text('Не нашел фотки, перезалей, пожалуйста')


commands = (
    ("start", start),
    ("help", help_cmd),
    ("group_add", group_add),
    ("group_list", group_list),
    ("group_user_add", group_user_add),
    ("group_user_del", group_user_del),
    ("photo", make_photo),
)


def main():
    global GROUPS
    global USER_NICKS
    global GROUP_WELCOME_MESSAGES
    global GROUP_WELCOME_MESSAGES

    logger.info('Starting up..')
    GROUPS = settings.load_json(settings.groups_file)
    GROUP_WELCOME_MESSAGES = settings.load_json(settings.group_welcomes_file)
    USER_NICKS = settings.load_json(settings.user_nicks_file)
    updater = Updater(
        settings.TOKEN,
        use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    for cmd_name, cmd_fn in commands:
        dp.add_handler(CommandHandler(cmd_name, cmd_fn))

    dp.add_handler(MessageHandler(
        Filters.status_update.new_chat_members, welcome))
    dp.add_handler(MessageHandler(Filters.text, any_text))
    dp.add_handler(MessageHandler(Filters.photo, sent_photo))
    dp.add_handler(
        MessageHandler(Filters.document.category("image"), file_photo))

    # log all errors
    dp.add_error_handler(error_handler)

    # Start the Bot
    updater.start_polling()

    # Block until you press Ctrl-C or the process receives SIGINT, SIGTERM or
    # SIGABRT. This should be used most of the time, since start_polling() is
    # non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()

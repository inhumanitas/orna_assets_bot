import base64
import sys
from dataclasses import dataclass

import requests

import re
import logging

import settings

logger = logging.getLogger(__name__)

MAX_IMAGE_SIZE = 1024


class ImageRecognitionError(Exception):
    pass


def search_attr(r, text, isint=True):
    res = ""
    g = re.search(r, text)
    if g is not None:
        res = g.groups()[0]
    res = res.strip()
    if isint:
        if len(res) > 4 or len(res) > 3 and res[0] != '1':
            if res.rfind('o') != -1:
                res = res[:res.rfind('o')]
            elif res.rfind('0') != -1:
                res = res[:res.rfind('0')]

        res = res.replace('o', '0')

    return res


@dataclass
class WeaponData:
    classes = {
        'Broken', 'Poor',
        'Famed', 'Superior', 'Legendary', 'Enchanted', 'Flaming',
        'Ornate', 'Masterforged', 'Demonforged', 'Godforged'
    }
    elements = {
        'Lightning', 'Fire', 'Earthen', 'Water', 'Holy', 'Dark'
    }

    name: str = ''
    hp: str = ''
    att: str = ''
    df: str = ''
    mag: str = ''
    res: str = ''
    mana: str = ''
    dex: str = ''
    ward: str = ''
    level: str = '1'
    quality: str = ''

    def data(self):
        j = {}
        j.update({'name': self.name})
        if self.hp != '': j.update({'hp': int(self.hp)})
        if self.att != '': j.update({'attack': int(self.att)})
        if self.df != '': j.update({'defense': int(self.df)})
        if self.mag != '': j.update({'magic': int(self.mag)})
        if self.res != '': j.update({'resistance': int(self.res)})
        if self.mana != '': j.update({'mana': int(self.mana)})
        if self.dex != '': j.update({'dexterity': int(self.dex)})
        if self.ward != '': j.update({'ward': int(self.ward)})
        if self.level != '': j.update({'level': int(self.level)})
        print(j)
        return j


class WeaponParser:
    item_class = WeaponData
    r_name = re.compile(r"\n([\w ']{3,})")
    r_hp = re.compile(r"((?<=HP[:z!2]) *[-0-9o]{0,4})")
    r_att = re.compile(r"((?<=A[tr]t[:z!2]) *[-0-9o]{0,4})")
    r_def = re.compile(r"((?<=Def[:z!2]) *[-0-9o]{0,4})")
    r_mag = re.compile(r"((?<=Mag[:z!2]) *[-0-9o]{0,4})")
    r_res = re.compile(r"((?<=Res[:z!2]) *[-0-9o]{0,4})")
    r_mana = re.compile(r"((?<=Mana[:z!2]) *[-0-9o]{0,4})")
    r_dex = re.compile(r"((?<=Dex[:z!2]) *[-0-9o]{0,4})")
    r_ward = re.compile(r"((?<=Ward[:z!2]) *[-0-9o]{0,3})")
    r_lvl = re.compile(r"((?<=Level) *[0-9o]{1,2})")
    r_lvl_alt = re.compile(
        r"\*[0-9o]{1,2}\W+([0-9o]{1,2})\W+[0-9o]{2}\/[0-9o]{2}\/2[0o][0-9o]{2}")
    r_lvl_exists = re.compile(r"\n(Level)\n")

    def __init__(self, text: str) -> None:
        super().__init__()
        self.text = text
        print(text)
        self.parse()

    def parse(self):
        name_grp = re.findall(self.r_name, self.text)
        name = name_grp[0]

        quality = 'Normal'

        for c in self.item_class.classes:
            if name.find(c) != -1:
                quality = c
                name = name[len(c) + 1:]
        for e in self.item_class.elements:
            pos = name.rfind(e)
            if (pos != -1) and (pos + len(e) == len(name)):
                name = name[:name.rfind(e) - 4]

        self.item = self.item_class(
            name=name,
            quality=quality,
            hp=search_attr(self.r_hp, self.text),
            att=search_attr(self.r_att, self.text),
            df=search_attr(self.r_def, self.text),
            mag=search_attr(self.r_mag, self.text),
            res=search_attr(self.r_res, self.text),
            mana=search_attr(self.r_mana, self.text),
            dex=search_attr(self.r_dex, self.text),
            ward=search_attr(self.r_ward, self.text),
            level=search_attr(self.r_lvl, self.text),
        )
        if self.item.level == "":
            # try alternate if level exists
            lvlexists = search_attr(self.r_lvl_exists, self.text, False)
            if lvlexists == 'Level':
                self.item.level = search_attr(self.r_lvl_alt, self.text)
            logger.info(
                "Alternative: '{}'; '{}'".format(lvlexists, self.item.level))


def cut_off_data(bytes_string):
    if (len(bytes_string)/1024) < MAX_IMAGE_SIZE:
        return bytes_string

    bytes_string = bytes_string[:round(2 * len(bytes_string) / 3)]
    return bytes_string


def parse_image(file_path):

    file_data = open(file_path, 'rb').read()

    file_data = cut_off_data(file_data)

    image_base64 = base64.b64encode(file_data)

    try:
        data = requests.post(
            settings.OCR_SPACE_API_URL,
            data={
                'apikey': settings.OCR_SPACE_API_KEY,
                'base64Image': b'data:image/jpeg;base64,' + image_base64,
            }
        )
    except requests.exceptions.ConnectionError:
        raise ImageRecognitionError(
            'Сервис распознавания недоступен, попробуй еще раз через минутку')

    data.raise_for_status()
    response_json = data.json()
    if response_json['IsErroredOnProcessing']:
        raise ImageRecognitionError(response_json['ErrorDetails'])

    text = response_json.get('ParsedResults')[0].get('ParsedText')
    weapon = WeaponParser(text).item
    return weapon


if __name__ == '__main__':
    print(parse_image(sys.argv[1]))

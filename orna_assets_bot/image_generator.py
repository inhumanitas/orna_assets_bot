import os

import settings


def create_image(media_group_id):
    import cv2
    import numpy as np

    media_path = settings.get_media_path(media_group_id)
    out_image_name = 'out.jpg'
    out_image_path = os.path.join(media_path, out_image_name)
    images = []
    for img_file in os.listdir(media_path):
        if img_file == out_image_name:
            continue

        file_path = os.path.join(media_path, img_file)
        img = cv2.imread(file_path)
        images.append(img)

    if images:
        images.reverse()
        vis = np.concatenate(images, axis=1)
        cv2.imwrite(out_image_path, vis)
        return out_image_path

import logging
import os
import sys
import requests

from image_recognition import parse_image, ImageRecognitionError

logger = logging.getLogger(__name__)


def get_data_by_image(file_path):
    try:
        image_data = parse_image(file_path)
    except ImageRecognitionError as e:
        raise ValueError(f'Could not parse {file_path}\n{e}')

    r = requests.post(
        'https://orna.guide/api/v1/assess', json=image_data.data())

    data = r.json()

    if 'error' in data:
        logger.critical(image_data)
        raise ValueError(data['error'])

    name = 'Name: {}\n'.format(data['name']) if 'name' in data else ''
    desc = 'Description: {}\n'.format(
        data['description']) if 'description' in data else ''
    tier = 'Tier: {}\n'.format(data['tier']) if 'tier' in data else ''
    type = 'Type: {}\n'.format(data['type']) if 'type' in data else ''
    boss = 'Boss: {}\n'.format(data['boss']) if 'boss' in data else ''
    qty = 'Quality: {} ({})\n'.format(
        image_data.quality,
        data.get('quality') or f'Quality: {image_data.quality}\n'
    )
    materials = 'Materials: {}\n'.format(
        ', '.join([a['name'] for a in data.get('materials', '')]))
    equip = 'Equipped by: {}\n'.format(
        ', '.join([a['name'] for a in data.get('equipped_by', '')]))
    drop = 'Dropped by: {}\n'.format(
        ', '.join([a['name'] for a in data.get('dropped_by', '')]))

    h = ['level']
    h.extend(list(data['stats']))
    headers = ''.join(['{:>' + str(len(i) + 1) + '}' for i in h]).format(
        *list(h))
    levels = {"level": {'base': 0, 'values': list(range(1, 13))}}
    levels.update(data['stats'])

    statstable = '\n'.join(
        [''.join(['{:>' + str(len(i) + 1) + '}' for i in h]) \
             .format(*[levels[stat]['values'][level] \
                       for stat in h]) \
         for level in range(12)])

    statslist = {
        "HP": image_data.hp, "Att": image_data.att, "Def": image_data.df,
        "Mag": image_data.mag, "Res": image_data.res, "Mana": image_data.mana,
        "Dex": image_data.dex, "Ward": image_data.ward
    }
    filtered_stats = {k: v for k, v in statslist.items() if v != ""}

    reply = "\n{}{}{}{}{}{}{}{}".format(name, desc, materials, tier, type,
                                        boss, equip, drop) + \
            ', '.join(k + ": " + v for k, v in
                      filtered_stats.items()) + "\nLevel: {}".format(image_data.level) + \
            "\n\n*ASSESSMENT*\n{}`{}\n{}`\n".format(qty, headers, statstable)
    return reply


if __name__ == '__main__':
    print(get_data_by_image(sys.argv[1]))

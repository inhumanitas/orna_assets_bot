from orna_assets_bot.settings import extend_groups

test_groups = {
    'group1': ['@u1', '@u2', '@u3'],
    'group2': ['@u1', '@u22'],
    'group3': []
}


def test_extend_groups_empty():
    result = extend_groups(
        test_groups,
        'Some text continue'
    )

    assert result == []

    result = extend_groups(
        test_groups,
        'Some text @no_group continue'
    )

    assert result == []


def test_extend_groups_simple():
    result = extend_groups(
        test_groups,
        'Some text @group1 continue'
    )

    assert result == ['Some text @group1 continue @u1 @u2 @u3']


def test_extend_groups_multiple():
    result = extend_groups(
        test_groups,
        'Some text @group1 continue by @group2 and finish'
    )

    assert result == [
        'Some text @group1 continue by @group2 and finish @u1 @u2 @u22 @u3'
    ]


def test_extend_groups_multi_list():
    result = extend_groups(
        test_groups,
        'Some text @group1 continue',
        1
    )

    assert result == [
        'Some text @group1 continue @u1',
        'Some text @group1 continue @u2',
        'Some text @group1 continue @u3'
    ]
    result = extend_groups(
        test_groups,
        'Some text @group1 continue',
        2
    )

    assert result == [
        'Some text @group1 continue @u1 @u2',
        'Some text @group1 continue @u3'
    ]


def test_extend_groups_multiple_multi_list():
    result = extend_groups(
        test_groups,
        'Some text @group1 continue by @group2 and finish',
        2
    )

    assert result == [
        'Some text @group1 continue by @group2 and finish @u1 @u2',
        'Some text @group1 continue by @group2 and finish @u22 @u3',
    ]

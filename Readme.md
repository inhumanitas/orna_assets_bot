# Install requirements

```bash
apt-get install python3-venv python3-opencv
```

# Prepare server directory

```bash
useradd orna
mkdir -p /opt/orna_assets_bot
chown -R orna /opt/orna_assets_bot
cd /opt/orna_assets_bot
python3 -m venv .env
source .env/bin/activate
pip install requirements.txt
```

# Copy service file

```bash
/etc/systemd/system/orna_assets_bot.service
```

# Go further

```bash
systemctl enable orna_assets_bot.service
systemctl start orna_assets_bot.service
```

# Logs

```bash
journalctl -fu orna_assets_bot.service
```

# Run local

###Pre-install
```bash
sudo apt install sshpass
sudo apt install virtualbox
sudo apt install vagrant
vagrant plugin install vagrant-hostmanager
```

###Create VM
```bash
cd provisioning
make vagrant
```

###Provide ssh key
```bash
cd provisioning
make ssh
```

###Deploy to VM
```bash
cd provisioning
make ansible
```

###Show changes
```bash
cd provisioning
make ansible-test
```

###Clean all and deploy to VM
```bash
cd provisioning
make all
```
